const express = require("express");
const app = express();
// Middleware to get user input
const bodyParser = require("body-parser");

let login = require("./public/login.json");

app.set("view engine", "ejs");
app.use(express.json());

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));

// app.get("/", (req, res) =>
// {
//     res.sendFile(__dirname + "/views/index.ejs");
// });


app.get("/", (req, res)=>
{
    res.render("index", {loginText: "Login"});
});

app.get("/about-us", (req, res) => {
    res.render("aboutUs");
});

app.get("/contact", (req, res) => {
    res.render("contact");
});

app.get("/work", (req, res)=>
{
    res.render("work");
});

app.get("/contact", (req, res) => {
    res.render("contact");
});

app.get("/sign", (req, res)=>
{
    res.render("sign");
});

app.get("/play", (req, res)=>
{
    res.render("play");
});
// For login GET
app.get("/login", (req, res) => {
    res.render("login", {errorText: ""});
});
// Get Data
// app.get("/api/v1/login", (req, res)=>
// {
//     res.status(200).json(login);
// });
// When user post data
app.post("/login", (req, res)=>
{
    // req.body. (point at "name" tag)
    let email = req.body.email;
    let password = req.body.password;
    // login is the json
    if (email === login.email && password === login.password) 
    {
        res.render("index", { loginText: `Welcome ${email}` });
    } else 
    {
        res.render("login", { errorText: "Invalid Email or Password" });
    }
      
});


app.listen(3000, () => {
    console.log('Server is running on port 3000');
});

