// player with choice and score
class Human
{
    constructor(name)
    {
        this.name = name;
    }
}

// Inherit Human name and make Competitor
class Competitor extends Human
{
    constructor(name, choice, score)
    {
        // super mean we pass the argument from the Human and use it here
        // if we dont put any arg in super (super()) it will pass the Human with the default Value "unefined or anyhting we put"
        super(name);
        this.choice = choice;
        this.score = score;
    }
    // if win use update score
    updateScore()
    {
        return ++this.score;
    }
    // random choice for COM
    randomChoice()
    {
        return this.choice = Game.choice[Math.floor(Math.random() * Game.choice.length)];
    }
}

// what the game are made of
class Game
{
    // Static for choice
    static choice = ["batu", "gunting", "kertas"];
    constructor()
    {
        this.player = new Competitor("Player", "", 0);
        this.com = new Competitor("COM", "", 0);
    }
    // if user click reset
    resetScore = () =>
    {
        this.player.score = 0;
        this.com.score = 0;
        console.log(`Game Reset Player: ${this.player.score} COM: ${this.com.score}`);
    }

    // show score 
    showScore = () =>
    {
        console.log(`Player: ${this.player.score}`)
        console.log(`COM: ${this.com.score}`)
    }
    
    // take the method and put it in here as new method in game
    updateScore = (competitor) =>
    {
       return competitor.updateScore();
    }

    // for COM Choice Random
    getComChoice = () =>
    {
        return this.com.randomChoice();
    }

}

// For restart
function restart()
{ 
    newGame.resetScore();
    // reset backgorund choice player
    const elements = document.querySelectorAll("#guntingPlayer, #batuPlayer, #kertasPlayer");
    elements.forEach((elements) =>{
        elements.style.backgroundColor = "";
        elements.style.padding = "0px";
    })
    // reset background choice com
    const elementsCOM = document.querySelectorAll("#guntingCOM, #batuCOM, #kertasCOM");
    elementsCOM.forEach((elementsCOM) => {
        elementsCOM.style.backgroundColor = "";
        elementsCOM.style.padding = "0px";
    })

    // reset vs
    const vs = document.getElementById("vs");
    vs.innerHTML = "VS";
    vs.style.backgroundColor = "";

}

function play(choice)
{
    // We get player choice from the argument we pass in the onclikc('choice') in html
    // Player
    let playerChoice = choice;

    // Declare to randomize COM
    // CPU
    let comChoice = newGame.getComChoice();

    console.log(`Player choose: ${playerChoice}`);
    console.log(`COM choose: ${comChoice}`);

    // reset vs background and html
    document.getElementById("vs").style.backgroundColor = "";

    if(playerChoice === 'gunting')
    {
        playerBackground(playerChoice);
        comBackground(comChoice);
        playerGunting(playerChoice, comChoice);
    }
    else if(playerChoice === 'batu')
    {
        playerBackground(playerChoice);
        comBackground(comChoice);
        playerBatu(playerChoice, comChoice);
    }
    else if(playerChoice == 'kertas')
    {
        playerBackground(playerChoice);
        comBackground(comChoice);
        playerKertas(playerChoice, comChoice);
    }
}

// function if draw
function draw(playerChoice, comChoice)
{
    if(playerChoice === comChoice)
    {
        console.log(`ITS A DRAW!!!`);
        newGame.showScore();
        vsDraw();
    }
}

// function if player choose gunting
function playerGunting(playerChoice, comChoice)
{
    // check if draw
    draw(playerChoice, comChoice);
    if(comChoice == 'kertas')
    {
        newGame.updateScore(newGame.player);
        console.log(`PLAYER WIN!!!`);
        newGame.showScore();
        vsPlayerWin();
    }
    else if (comChoice === 'batu')
    {
        newGame.updateScore(newGame.com);
        console.log(`CPU WIN!!!`);
        newGame.showScore();
        vsComWin();
    }
}

// function if player choose batu
function playerBatu(playerChoice, comChoice)
{
    // check if draw
    draw(playerChoice, comChoice);
    if(comChoice == 'kertas')
    {
        newGame.updateScore(newGame.com);
        console.log(`COM WIN!!!`);
        newGame.showScore();
        vsComWin();
    }
    else if (comChoice === 'gunting')
    {
        newGame.updateScore(newGame.player);
        console.log(`PLAYER WIN!!!`);
        newGame.showScore();
        vsPlayerWin();
    }
}

// function if player choose kertas
function playerKertas(playerChoice, comChoice)
{
    draw(playerChoice, comChoice);
    if(comChoice == 'gunting')
    {
        newGame.updateScore(newGame.com);
        console.log(`COM WIN!!!`);
        newGame.showScore();
        vsComWin();
    }
    else if (comChoice === 'batu')
    {
        newGame.updateScore(newGame.player);
        console.log(`PLAYER WIN!!!`);
        newGame.showScore();
        vsPlayerWin();
    }
}

function playerBackground(playerChoice)
{
    // we reset background for player
    const elements = document.querySelectorAll("#guntingPlayer, #batuPlayer, #kertasPlayer");
    elements.forEach((elements) =>{
        elements.style.backgroundColor = "";
        elements.style.padding = "0px";
    })

    if(playerChoice === 'batu')
    {
        const backgroundPlayer = document.getElementById("batuPlayer");
        backgroundPlayer.style.backgroundColor = "#C4C4C4";
        backgroundPlayer.style.padding = '13px';
    }
    else if(playerChoice === 'gunting')
    {
        const backgroundPlayer = document.getElementById("guntingPlayer");
        backgroundPlayer.style.backgroundColor = "#C4C4C4";
        backgroundPlayer.style.padding = '13px';
    }
    else if(playerChoice === 'kertas')
    {
        const backgroundPlayer = document.getElementById("kertasPlayer");
        backgroundPlayer.style.backgroundColor = "#C4C4C4";
        backgroundPlayer.style.padding = '13px';
    }
}

function comBackground(comChoice)
{
    //Reset background color and padding COM
    const elements = document.querySelectorAll("#guntingCOM, #batuCOM, #kertasCOM");
    elements.forEach((elements) => {
        elements.style.backgroundColor = "";
        elements.style.padding = "0px";
    })

    // we change com backround whicever they pick
    if(comChoice === 'batu')
    {
        const backgroundCOM = document.getElementById("batuCOM");
        backgroundCOM.style.backgroundColor = "#C4C4C4";
        backgroundCOM.style.padding = '13px';
        
    }
    else if(comChoice === 'gunting')
    {
        const backgroundCOM  = document.getElementById("guntingCOM");
        backgroundCOM.style.backgroundColor = "#C4C4C4";
        backgroundCOM.style.padding = '13px';
    }
    else if(comChoice === 'kertas')
    {
        const backgroundCOM  = document.getElementById("kertasCOM");
        backgroundCOM.style.backgroundColor = "#C4C4C4";
        backgroundCOM.style.padding = '13px';
    }
}


function vsDraw()
{
    const vs = document.getElementById("vs");
    vs.innerHTML = "DRAW"
    vs.style.backgroundColor = "#4C9654";
}

function vsPlayerWin()
{
    const vs = document.getElementById("vs");
    vs.innerHTML = "PLAYER WIN!!!"
    vs.style.backgroundColor = "#4C9654";
}

function vsComWin()
{
    const vs = document.getElementById("vs");
    vs.innerHTML = "COM WIN!!!"
    vs.style.backgroundColor = "#4C9654";
}



var newGame = new Game();
